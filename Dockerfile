FROM node:12.4-alpine

COPY package.json /opt/microservices/
RUN cd /opt/microservices; npm install

COPY ./src/ /opt/microservices/src/

ARG service_version
ENV SERVICE_VERSION ${service_version:-v1}

EXPOSE 3001
WORKDIR /opt/microservices

CMD npm run prod
