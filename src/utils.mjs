import pino from 'pino';
import Redis from 'express-redis-cache';
import { promisify } from 'util';
import config from './config';

export const getLog = () => {
  if (global.singletonLog === undefined) {
    global.singletonLog = pino({
      enabled: config.logging.ENABLED,
      level: config.logging.LOG_LEVEL,
      base: null,
      prettyPrint: config.environment === 'development'
    });
  }
  return global.singletonLog;
};

export const getCache = () => {
  if (global.singletonCache === undefined) {
    const log = getLog();
    const redisConfig = {
      host: config.cache.HOST,
      port: config.cache.PORT,
      auth_pass: config.cache.PASS,
      prefix: config.cache.PREFIX
    };
    const cache = Redis(redisConfig);
    cache.getAsync = promisify(cache.get);
    cache.addAsync = promisify(cache.add);

    cache.on('message', (message) => {
      log.debug(`CACHE: ${message}`);
    });

    cache.on('error', (error) => {
      log.error(`CACHE: ${error}`);
    });
    global.singletonCache = cache;
  }
  return global.singletonCache;
};

export const checkEnvSetup = (envVars) => {
  const notSet = envVars.filter(envVar => !process.env[envVar]);
  return !(notSet && notSet.length > 0);
};
