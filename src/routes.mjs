import express from 'express';
import accountsController from './client/controllers/AccountController';
import transactionController from './client/controllers/TransactionController';
import infoApi from './client/info';
import config from './config';
import { institutionInfo } from './client/infoConfig';
import { getLog } from './utils';

// const transactionController =  new TransactionController();

const router = express.Router();
const log = getLog();

router.use((req, res, next) => {
  log.debug('Request received', req.originalUrl);
  next();
});


// health check
router.get(`/api/banking/${config.apiVersion}/${institutionInfo.institutionId}/health/:name`, (req, res) => {
  res.send(req.params);
});

// info
router.get(`/api/banking/${config.apiVersion}/${institutionInfo.institutionId}/info`, infoApi.getInfo);

router.get(`/api/banking/${config.apiVersion}/${institutionInfo.institutionId}/tokenInfo`, infoApi.getTokenInfo);

// account list
router.get(`/api/banking/${config.apiVersion}/${institutionInfo.institutionId}/accounts`, accountsController.list);

// details of one account
router.get(`/api/banking/${config.apiVersion}/${institutionInfo.institutionId}/accounts/:iban`, accountsController.detail);

// transaction list
router.get(`/api/banking/${config.apiVersion}/${institutionInfo.institutionId}/transactions`, transactionController.allAccountsList);

// details of one transaction
router.get(`/api/banking/${config.apiVersion}/${institutionInfo.institutionId}/transactions/:transId`, transactionController.detail);

// transaction list of one account
router.get(`/api/banking/${config.apiVersion}/${institutionInfo.institutionId}/accounts/:iban/transactions`, transactionController.list);

// details of one transaction
// router.get(
// `/api/banking/${config.apiVersion}/${institutionInfo.institutionId}/accounts/:iban/transactions/:transId`,
// transactionsApi.transactionDetail
// );


export default router;
