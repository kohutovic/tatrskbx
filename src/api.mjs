import pEvent from 'p-event';
import { getLog, checkEnvSetup } from './utils';
import config from './config';
import startServer from './server';
import app from './expressApp';
import tatraHttpException from './client/tatraHttpException';

const runApiService = async () => {
  const SERVER_TERMINATION_SIGNALS = ['SIGINT', 'SIGHUP', 'SIGTERM'];
  const BANK_API_ENV_VARS = ['API_URL', 'LOGIN_URL', 'TOKEN_URL', 'REFRESH_TOKEN_URL', 'REVOKE_TOKEN_URL'];
  const { HOST, PORT } = config.server;
  let server = null;
  let log;
  try {
    log = getLog();
    if (!checkEnvSetup(BANK_API_ENV_VARS)) {
      throw Error(`Environment variables need to be set: ${BANK_API_ENV_VARS}`);
    }

    server = await startServer(app, HOST, PORT);
    log.info(`Everifin server is running on: ${HOST}:${PORT}`);
    log.debug(`Process ID (PID) = ${process.pid}`);

    // TODO: setup server monitoring with e.g. sentry tool

    SERVER_TERMINATION_SIGNALS.forEach((signal) => {
      process.on(signal, (sig) => {
        log.debug(`Termination signal received (${sig}). Graceful shutdown start.`);
      });
    });

    // Keep running until one of termination signals appear.
    // In case of 'uncaughtException' or 'unhandledRejection' events,
    // promise will be rejected and catch block will be executed.
    await pEvent(process, SERVER_TERMINATION_SIGNALS, {
      rejectionEvents: ['uncaughtException', 'unhandledRejection']
    });
  } catch (err) {
    if (!(err instanceof tatraHttpException)) {
      process.exitCode = 1;
      log.fatal(err);
    }
  } finally {
    if (server) {
      log.info('Everifin server is shutting down...');
      await server.stop();
      log.info('Done');
    }

    // This takes a chance to potentional activity keeping the event loop running
    // to finish before exiting node.js process.
    // If there is no other such activity the process may exit before process.exit() is invoked.
    setTimeout(process.exit, 10000).unref();
  }
};

runApiService();
