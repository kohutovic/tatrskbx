const config = {
  apiVersion: 'v1',
  environment: process.env.NODE_ENV || 'development',
  server: {
    HOST: process.env.HOST || '0.0.0.0',
    PORT: parseInt(process.env.PORT, 10) || 3001
  },
  logging: {
    ENABLED: process.env.ENABLE_LOGS ? process.env.ENABLE_LOGS === 'true' : true,
    LOG_LEVEL: process.env.LOG_LEVEL || 'info'
  },
  cache: {
    HOST: process.env.REDIS_HOST,
    PORT: process.env.REDIS_PORT,
    PASS: process.env.REDIS_PASS,
    PREFIX: process.env.REDIS_PREFIX,
    expiry: {
      ROUTE_INFO: 60 * 60 * 8 // 8 hours
    }
  },
  bankApi: {
    API_URL: process.env.API_URL,
    LOGIN_URL: process.env.LOGIN_URL,
    TOKEN_URL: process.env.TOKEN_URL,
    REFRESH_TOKEN_URL: process.env.REFRESH_TOKEN_URL,
    REVOKE_TOKEN_URL: process.env.REVOKE_TOKEN_URL
  }
};

export default config;
