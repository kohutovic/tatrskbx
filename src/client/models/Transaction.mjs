import ValueObject from 'value-object';
import AccountReference from './AccountReference';

class Transaction extends ValueObject.define({
  id: 'string',
  type: 'string?',
  datetime: 'string?',
  currency: 'string',
  amount: 'number',
  status: 'string?',
  counterAccount: AccountReference,
  customerReference: 'string?',
  notes: 'string?'
}) {}

export default Transaction;
