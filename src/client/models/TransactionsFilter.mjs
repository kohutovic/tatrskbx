import ValueObject from 'value-object';


class TransactionsFilter extends ValueObject.define({
  iban: 'string',
  dateFrom: 'string?',
  dateTo: 'string?',
  page: 'number?',
  pageSize: 'number?'
}) {}

export default TransactionsFilter;
