import ValueObject from 'value-object';


class Account extends ValueObject.define({
  iban: 'string',
  institutionId: 'string',
  institutionName: 'string',
  systemId: 'string',
  accountName: 'string',
  currency: 'string',
  balance: 'number',
  availableBalance: 'number'
}) {}

export default Account;
