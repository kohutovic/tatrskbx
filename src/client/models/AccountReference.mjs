import ValueObject from 'value-object';


class AccountReference extends ValueObject.define({
  iban: 'string',
  name: 'string?'
}) {}

export default AccountReference;
