import config from '../config';

const { bankApi } = config;

export const institutionInfo = {

  institutionApiUrl: bankApi.API_URL,
  institutionApiVersion: 'v1',
  institutionType: 'bank',
  institutionName: 'Tatra Banka',
  institutionId: 'tatrskbx',
  institutionCountryName: 'Cyprus',
  institutionCountryCode: 'CYP',
  institutionUrl: 'https://www.tatrabanka.sk',
  institutionLogo: 'tba',
  loginRequest: {
    url: bankApi.LOGIN_URL,
    path: '/oauth2/auth',
    type: 'redirect',
    query: {
      response_type: 'code',
      client_id: '{{app_bank_client_id}}',
      redirect_uri: '{{redirect_uri}}',
      scope: 'AISP',
      state: '{{state}}'
    },
    authUserFields: ['Business ID', 'User ID', 'Password'],
    response: {
      type: 'redirect',
      queryFields: {
        codeField: 'code',
        stateField: 'state'
      }
    }
  }
};

export const tokenInfo = {
  everifinApiUsage: {
    headers: {
      Authorization: 'Bearer {{everifin_access_token}}',
      'tatrskbx-at': '{{user_bank_access_token}}',
      'tatrskbx-cid': '{{app_bank_client_id}}'
    }
  },
  accessTokenRequest: {
    url: bankApi.TOKEN_URL,
    pathname: '/token/exchange',
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      Authorization: 'Basic {{base64encoded_client_id_client_secret}}'
    },
    body: {
      grant_type: 'authorization_code',
      redirect_uri: '{{redirect_uri}}',
      code: '{{code}}'
    },
    response: {
      type: 'json',
      fields: {
        tokenTypeField: 'token_type',
        accessTokenField: 'access_token',
        clientIdField: 'client_id',
        scopeField: 'scope',
        createdOnField: 'created_on',
        expiresAtField: 'expires_at',
        refreshTokenField: 'refresh_token'
      }
    }
  },

  refreshTokenRequest: {
    url: bankApi.REFRESH_TOKEN_URL,
    path: '/token',
    method: 'POST',
    headers: {
      Authorization: 'Basic {{base64encoded_client_id_client_secret}}'
    },
    query: {
      grant_type: 'refresh_token',
      refresh_token: '{{user_bank_refresh_token}}'
    },
    response: {
      type: 'json',
      fields: {
        tokenTypeField: 'token_type',
        accessTokenField: 'access_token',
        clientIdField: 'client_id',
        scopeField: 'scope',
        createdOnField: 'created_on',
        expiresAtField: 'expires_at',
        refreshTokenField: 'refresh_token'
      }
    }
  },
  revokeTokenRequest: {
    url: bankApi.REVOKE_TOKEN_URL,
    path: '/token',
    method: 'DELETE',
    headers: {
      Authorization: 'Bearer {{user_bank_access_token}}'
    },
    response: {
      type: 'json',
      fields: {
        payloadField: 'payload',
        errorsField: 'errors'
      }
    }
  }
};
