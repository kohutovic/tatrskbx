import fetch from 'node-fetch';
import uuid from 'uuid/v1';
import TatraHttpException from '../tatraHttpException';
import { institutionInfo } from '../infoConfig';
import { getLog } from '../../utils';

const log = getLog();

const createHeaders = accessToken => ({
  'Content-Type': 'application/json; charset=utf-8',
  Authorization: `Bearer ${accessToken}`,
  'Request-ID': uuid()
});


async function sendRequest(options, url) {
  log.debug(options);
  log.debug(url);
  const response = await fetch(url, options);
  const body = await response.json();
  log.debug(body);

  if (response.status < 300) {
    return body;
  }

  throw new TatraHttpException(`${institutionInfo.institutionName}: Error at ${url}`, response.status, body);
}

async function get(url, accessToken) {
  const options = {
    headers: createHeaders(accessToken)
  };
  return sendRequest(options, url);
}

async function post(url, accessToken, payload) {
  const options = {
    method: 'POST',
    body: JSON.stringify(payload),
    headers: createHeaders(accessToken)
  };
  return sendRequest(options, url);
}


export default {
  get, post
};
