import url from 'url';
import TatraHttpException from '../tatraHttpException';
import { institutionInfo } from '../infoConfig';
import tatraClient from './tatraHttpClient';
import Account from '../models/Account';

function assembleUri(endpoint) {
  return url.format({
    host: institutionInfo.institutionApiUrl,
    pathname: `/${institutionInfo.institutionApiVersion}/${endpoint}`
  });
}

function mapAccount(accountInfo, accountDetail) {
  return new Account({
    institutionId: accountInfo.servicer.financialInstitutionIdentification,
    institutionName: institutionInfo.institutionName,
    systemId: accountInfo.identification.iban,
    accountName: accountInfo.name,
    iban: accountInfo.identification.iban,
    currency: accountInfo.baseCurrency,
    balance: accountDetail.balances.find(element => element.typeCodeOrProprietary === 'ITBD').amount.value,
    availableBalance: accountDetail.balances.find(element => element.typeCodeOrProprietary === 'ITAV').amount.value
  });
}

const getAccountDetail = async (accessToken, iban) => tatraClient.post(assembleUri('accounts/information'), accessToken, { iban });

const getAccountList = async (accessToken) => {
  const response = await tatraClient.get(assembleUri('accounts'), accessToken);

  if (response.accounts) {
    const accountListRemapped = response.accounts.map(async (account) => {
      const accountDetail = await getAccountDetail(accessToken, account.identification.iban);
      return mapAccount(account, accountDetail);
    });

    return Promise.all(accountListRemapped);
  }

  throw new TatraHttpException(`${institutionInfo.institutionName}: Response body's payload has no 'accounts' member.`, 500, response);
};


export default {
  getAccountList,
  getAccountDetail
};
