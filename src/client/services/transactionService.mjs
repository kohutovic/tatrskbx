import moment from 'moment';
import TatraHttpException from '../tatraHttpException';
import { assembleUri } from '../clientUtils';
import { institutionInfo } from '../infoConfig';
import tatraClient from './tatraHttpClient';
import Transaction from '../models/Transaction';
import AccountReference from '../models/AccountReference';

function mapTransaction(transactionData) {
  const txInfo = {
    id: transactionData.transactionDetails.references.transactionIdentification,
    type: transactionData.bankTransactionCode,
    datetime: moment(transactionData.valueDate, 'YYYYMMDDHHmmss')
      .format(),
    currency: transactionData.amount.currency,
    customerReference: transactionData.customerReference,
    status: transactionData.status,
    notes: transactionData.transactionDetails.additionalTransactionInformation
  };

  if (transactionData.creditDebitIndicator === 'DBIT') {
    txInfo.counterAccount = new AccountReference({
      name: transactionData.transactionDetails.relatedParties.creditor.name,
      iban: transactionData.transactionDetails.relatedParties.creditorAccount.identification
    });
    txInfo.amount = -1 * transactionData.amount.value;
  } else {
    txInfo.counterAccount = new AccountReference({
      name: transactionData.transactionDetails.relatedParties.debtor.name,
      iban: transactionData.transactionDetails.relatedParties.debtorAccount.identification
    });
    txInfo.amount = transactionData.amount.value;
  }
  return new Transaction(txInfo);
}

const getTransactionList = async (accessToken, filter) => {
  const response = await tatraClient.post(assembleUri('accounts/transactions'), accessToken, filter.toPlainObject());

  if (response.transactions) {
    return response.transactions.map(transaction => mapTransaction(transaction));
  }

  throw new TatraHttpException(`${institutionInfo.institutionName}: Response body's payload has no 'accounts' member.`, 500, response);
};


const getTransactionDetail = async (accessToken, iban) => tatraClient.post(assembleUri('accounts/information'), accessToken, { iban });

export default {
  getTransactionList,
  getTransactionDetail
};
