import { institutionInfo, tokenInfo } from './infoConfig';


const getInfo = async (req, res) => {
  const result = {};
  result.type = 'institution-info';
  result.status = 'success';
  result.data = institutionInfo;
  await res.json(result);
};

const getTokenInfo = async (req, res) => {
  const result = {};
  result.type = 'token-info';
  result.status = 'success';
  const respData = {};
  respData.institutionId = institutionInfo.institutionId;
  result.data = Object.assign(respData, tokenInfo);
  await res.json(result);
};

export default {
  getInfo,
  getTokenInfo
};
