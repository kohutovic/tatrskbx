import url from 'url';
import moment from 'moment';
import { institutionInfo } from './infoConfig';
import TransactionsFilter from './models/TransactionsFilter';

export function responseObject(type, status, data) {
  const result = {};
  result.type = type;
  result.status = status;
  result.data = data;

  return result;
}

export function assembleUri(endpoint) {
  return url.format({
    host: institutionInfo.institutionApiUrl,
    pathname: `/${institutionInfo.institutionApiVersion}/${endpoint}`
  });
}

export function getAccessToken(req) {
  const bankIdLowerCase = institutionInfo.institutionId.toLowerCase();

  return req.headers[`${bankIdLowerCase}-at`];
}

export function builtQueryFilter(req, iban) {
  const format = 'YYYY-MM-DD';
  const filter = {};
  filter.dateFrom = moment()
    .format(format);
  filter.dateTo = filter.dateFrom;
  filter.iban = iban || req.params.iban;

  if (req.query.date) {
    filter.dateFrom = moment(req.query.date, 'YYYYMMDD')
      .format(format);
  }

  if (req.query.dateTo) {
    filter.dateTo = moment(req.query.dateTo, 'YYYYMMDD')
      .format(format);
  }

  if (req.query.page) {
    filter.page = parseInt(req.query.page, 10);
  }

  if (req.query.pageSize) {
    filter.pageSize = parseInt(req.query.pageSize, 10);
  }

  return new TransactionsFilter(filter);
}
