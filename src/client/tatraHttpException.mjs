export default class TatraHttpException {
  constructor(message, responseStatus = 500, data = null) {
    this.message = message;
    this.responseStatus = responseStatus;
    this.tatra_error_code = data.errorCode;
    this.tatra_error_message = data.errorDescription;
  }
}
