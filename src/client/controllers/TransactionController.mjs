import TransactionService from '../services/transactionService';
import AccountService from '../services/accountService';
import { builtQueryFilter, getAccessToken, responseObject } from '../clientUtils';
import { getLog } from '../../utils';

const log = getLog();


class TransactionController {
  list = async (req, res, next) => {
    log.debug('Getting transaction list...');
    try {
      const filter = builtQueryFilter(req);
      const accessToken = getAccessToken(req);

      const accountDetails = await AccountService.getAccountDetail(accessToken, filter.iban);
      accountDetails.transactions = await TransactionService.getTransactionList(accessToken, filter);

      log.debug(accountDetails);
      res.status(200)
        .json(responseObject('account-transactions', 'success', accountDetails));
    } catch (e) {
      next(e);
    }
  };

  detail = async (req, res, next) => {
    log.debug('Getting transaction detail...');
    try {
      const accountInfo = await AccountService.getAccountDetail(getAccessToken(req), req.params.iban);
      res.status(200)
        .json(responseObject('account', 'success', accountInfo));
    } catch (e) {
      next(e);
    }
  };

  allAccountsList = async (req, res, next) => {
    log.debug('Getting ALL transaction detail...');
    try {
      const accessToken = getAccessToken(req);
      const accountList = await AccountService.getAccountList(accessToken);
      Promise.all(accountList.map(
        account => TransactionService.getTransactionList(accessToken, builtQueryFilter(req, account.iban))
      ))
        .then(
          (result) => {
            res.status(200)
              .json(responseObject('all-transaction', 'success', result.flat()))
              .send();
          }
        );
    } catch (e) {
      next(e);
    }
  };
}

export default new TransactionController();
