import AccountService from '../services/accountService';
import { getAccessToken, responseObject } from '../clientUtils';
import { getLog } from '../../utils';

const log = getLog();


class AccountController {
  list = async (req, res, next) => {
    log.debug('Getting account list...');
    try {
      const accountList = await AccountService.getAccountList(getAccessToken(req));

      log.debug(accountList);
      res.status(200)
        .json(responseObject('account-list', 'success', accountList));
    } catch (e) {
      next(e);
    }
  };

  detail = async (req, res, next) => {
    log.debug('Getting account detail...');
    try {
      const accountInfo = await AccountService.getAccountDetail(getAccessToken(req), req.params.iban);
      res.status(200)
        .json(responseObject('account', 'success', accountInfo));
    } catch (e) {
      next(e);
    }
  };
}

export default new AccountController();
