import util from 'util';
import http from 'http';
import stoppable from 'stoppable';
import pEvent from 'p-event';


async function startServer(app, host, port) {
  const CLOSE_CONNECTIONS_DELAY = 7000; // Milliseconds to wait before force-closing connections

  // Stoppable stops accepting new connections and closes existing, idle connections (including keep-alives)
  // without killing requests that are in-flight (this is useful in case of graceful shutdown)
  const server = stoppable(http.createServer(app), CLOSE_CONNECTIONS_DELAY);

  server.listen(port, host);
  server.stop = util.promisify(server.stop);

  await pEvent(server, 'listening'); // Wait for 'listening' event as if it were promise

  return server;
}


export default startServer;
