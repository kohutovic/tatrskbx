import express from 'express';
import bodyParser from 'body-parser';
import { getLog } from './utils';
import routes from './routes';
import tatraHttpException from './client/tatraHttpException';

const log = getLog();
const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Use routes
app.use('/', routes);

// Catch 404 and forward to error handler.
app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.uri = req.originalUrl;
  err.responseStatus = 404;
  next(err);
});

// Error handler
app.use((err, req, res, next) => {
  log.error(err.message, err.stack);
  const response = err instanceof tatraHttpException ? err : { message: err.message, stack: err.stack };
  res.status(err.responseStatus || 500).json(response).send();
  next();
});

export default app;
